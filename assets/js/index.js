// déclarer une variable
let maVariable= 42;
// var maVariable2 = 43; à éviter

// déclarer une constante -> toujours initialiser
const MA_CONSTANTE = 44;

// type number
let monNombre = 45.56;

// type string -> 3 syntaxes "", '',`` (pour concaténer ${})
let maChaine = "ma Chaine";

// type tableau -> on peut mettre ce qu'on veut dedans
let tableau = [55, 'string', []];

// type dictionnaire
let dico = {
    maCle : 'maValeur',
    'maCle2' : 45
}

// type fonction
let fonction = function(){};
let fonction2 = () => {};
function maFonction() {
    // pas de paramètres de retour à la déclaration de la fonction
}

// type boolean
let monBooleen = true;

// sortie utilisateur (affiche quelque chose dans la console du browser)
console.log(42); //equivalent de console.writeline
console.table(tableau); // prend un tableau en paramètre et l'affiche avec l'index et la valeur
console.log(tableau);

// entrée untilisateur
let age = prompt("Quel est votre âge ?") // Ouvre une boite d'entrée
age = parseInt(age);
console.log(age + 5);

// structure conditionnelle
// if() {

// }

// Boucles
// for(let i = 0; i < 5; i++){

// }
// while(true){

// }

// === vérifie la valeur et le type de la variable
// Nan -> not a number