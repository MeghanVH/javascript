let p_list = document.querySelectorAll('p');
let h_list = document.querySelectorAll('.container1 h2');
let select = document.getElementById('sel_cat');
let commande = document.getElementById('commande')
let menuListe = document.getElementsByClassName('menu')
// Cacher les plats non sélectionnés
select.addEventListener('change', e =>{
    let selection = select.value;
    for(let p of p_list){
        p.classList.add('hidden');
        if(p.classList.contains(selection) || selection == ""){
            p.classList.remove('hidden');
        }
    }
    for(let h of h_list){
        h.classList.add('hidden');
        if(h.classList.contains(selection) || selection == ""){
            h.classList.remove('hidden');
        }
    }
});
let supprimer = event => {
    let object = event.target;
    li = object.parentNode;
    commande.removeChild(li);
}
// Ajout à une liste quand on clique sur le plat
for(let menu of menuListe) {
    menu.addEventListener('click', e => {
        let li = document.createElement('li');
        let numb = document.createElement('input');
        numb.setAttribute('type', 'number');
        numb.setAttribute('value', '1');
        let suppr = document.createElement('button');
        suppr.innerText = '-';
        li.innerText = e.target.innerText;
        li.appendChild(suppr);
        li.appendChild(numb);
        suppr.addEventListener('click', supprimer);
        commande.appendChild(li);
    });
}

